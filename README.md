# Wavee

Wavee is an interactive platform where music lovers can create posts, share opinions, and engage with albums and playlists through likes and comments. Integrated with the Spotify API, it provides a dynamic space for users to connect and discuss their favorite tunes.

## Building & Running

Wavee can be built and run locally or deployed using Google Cloud Run.

### Locally

Begin by installing `PostgreSQL`. Follow the installation guide [here](https://www.postgresql.org/download/).

#### Frontend Setup (`./client`)

1. Install dependencies:

   ```bash
   npm install
   ```

2. Configure the environment variables in the `.env` file located at `/client/.env`:

   ```
   VITE_PORT=<YOUR_LOCAL_PORT>
   ```

3. Start the application:
   ```bash
   npm start
   ```

#### Backend Setup (`./server`)

1. Install dependencies:

   ```bash
   npm install
   ```

2. Set up the environment by updating the `.env` file at `/server/.env`:

   ```
   SPOTIFY_CLIENT_ID=<YOUR_SPOTIFY_CLIENT_ID>
   DB_LOCAL_USER=<YOUR_LOCAL_USER>
   DB_LOCAL_PASSWORD=<YOUR_LOCAL_PASSWORD>
   DB_LOCAL_DATABASE=<YOUR_LOCAL_DATABASE>
   DB_LOCAL_HOST=<YOUR_LOCAL_HOST>
   DB_LOCAL_PORT=<YOUR_LOCAL_PORT>
   ```

3. Launch the backend in development mode, which uses the local PostgreSQL database settings from your `.env` file:
   ```bash
   npm run dev
   ```

### Google Cloud Run

1. Update cloud-specific settings in the `.env` file located at `/server/.env`:

   ```
   URL=<YOUR_CLOUD_BACKEND_URL>
   SPOTIFY_CLIENT_ID=<YOUR_SPOTIFY_CLIENT_ID>
   DB_CLOUD_USER=<YOUR_CLOUD_USER>
   DB_CLOUD_PASSWORD=<YOUR_CLOUD_PASSWORD>
   DB_CLOUD_DATABASE=<YOUR_CLOUD_DATABASE>
   DB_CLOUD_INSTANCE_CONNECTION_NAME=<YOUR_CLOUD_INSTANCE_CONNECTION_NAME>
   ```

2. On Google Cloud Build, ensure the variables `_PROJECT_ID`, `_FRONTEND_NAME`, `_BACKEND_NAME`, `_FRONTEND_APP`, `_BACKEND_APP`, and `_VERSION` are correctly configured.

3. Initiate a build and deploy to Google Cloud Run using Cloud Build.

4. Once the deployment is complete, verify the running instance on Google Cloud Run.

## CI/CD Architecture

For a visual representation of the CI/CD process, refer to the diagram below:

![CI/CD](server/CI:CD.png "CI/CD")
