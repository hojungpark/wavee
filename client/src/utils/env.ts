export const getEnv = (name: string) => {
    const value = import.meta.env[name];
    if (value == null) {
        throw new Error(`Could not get environment variable '${name}'`);
    }

    return value;
};
