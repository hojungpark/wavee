namespace response {
    export interface Post {
        id: number;
        title: string;
        body?: string;
        modified_at: string;
        spotify_user_id: string;
    }

    export interface Comments {
        id: number;
        body: string;
        modified_at: string;
    }
}

export type { response };
