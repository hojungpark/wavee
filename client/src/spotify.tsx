export const authEndPoint = 'https://accounts.spotify.com/authorize';

const redirectUri = 'http://localhost:5173/';

const clientId = '';

const scopes = [
    'user-read-currently-playing',
    'user-read-recently-played',
    'user-read-playback-state',
    'user-top-read',
    'user-modify-playback-state'
]