import axios from 'axios';
import { useState } from 'react';
import { getEnv } from '../utils/env';
import { useNavigate } from 'react-router-dom';

const PostForm = () => {
    const navigate = useNavigate();
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const url = getEnv('DEV') ? `http://localhost:${getEnv('VITE_PORT')}` : getEnv('VITE_URL');
    const handleCancel = () => {
        if (window.confirm('Are you cure you want to delete this post?')) {
            window.location.href = '/post-list';
        }
    };

    const handleSubmit = () => {
        // Prepare the data
        const postData = {
            title: title,
            body: body,
            spotify_user_id: 'user_4', // TODO: Replace with actual user ID
        };

        // Use axios to send a POST request
        axios
            .post(`${url}/api/community/posts/`, postData)
            .then((response) => {
                console.log(response.data);
                // Optionally, redirect or clear the form here
            })
            .catch((error) => {
                console.error('Error posting new post:', error);
                // Handle error
            });
        navigate('/post-list');
    };

    return (
        <div className="bg-dark tracking-wide h-screen w-screen">
            <div className="pt-3 font-WAVEE3 heading text-center text-2xl m-5 text-light">
                New Post
                <div className="font-bodytext3 text-lg font-light editor mx-auto w-10/12 flex flex-col text-light p-4 shadow-lg max-w-2xl">
                    <input
                        className="bg-dark title border border-light-blue p-2 mb-4 outline-none"
                        spellCheck="false"
                        placeholder="Title"
                        type="text"
                        value={title}
                        onChange={(e) => setTitle(e.target.value)}
                    />
                    <textarea
                        className="bg-dark description sec p-3 h-60 border border-light-blue outline-none"
                        spellCheck="false"
                        placeholder="Write away!"
                        value={body}
                        onChange={(e) => setBody(e.target.value)}
                    ></textarea>

                    {/* buttons */}
                    <div className="buttons flex pt-3">
                        <div
                            className="btn bg-red-400 p-1 px-4 mr-3 cursor-pointer text-light ml-auto"
                            onClick={handleCancel}
                        >
                            Cancel
                        </div>
                        <div
                            className="btn border border-light-blue p-1 px-4 cursor-pointer text-light"
                            onClick={handleSubmit}
                        >
                            Post
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PostForm;
