import { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { getEnv } from '../utils/env';
import { response } from '../utils/responses';
import { format } from '../utils/format';

const Home = () => {
    const [posts, setPosts] = useState<response.Post[]>([]);
    const url = getEnv('DEV') ? `http://localhost:${getEnv('VITE_PORT')}` : getEnv('VITE_URL');
    useEffect(() => {
        axios
            .get<response.Post[]>(`${url}/api/community/posts`)
            .then((response) => {
                setPosts(response.data); // Set posts with response data
                console.log('it is working I think');
            })
            .catch((error) => console.error('Error fetching posts:', error));
    }, []);

    let postsArray = Array.from(posts);

    const albumCovers = [
        'https://s3.eu-west-1.amazonaws.com/blog.mixtapemadness.com/wp-content/uploads/20211120123736/Kojey-Radical-2-768x768.jpg',
        'https://granitebaytoday.org/wp-content/uploads/2023/08/Zach-bryan.jpg',
        'https://cdn.musebycl.io/2022-07/Joni_Mitchell_Blue_1500.jpg',
        'https://i.cbc.ca/1.4574015.1614648535!/fileImage/httpImage/image.jpg_gen/derivatives/original_780/daniel-caesar.jpg',
    ];

    let albumCoversList: any[] = [];

    albumCovers.forEach((albumCover, index) => {
        albumCoversList.push(
            <div key={index}>
                <div className="relative group">
                    <img
                        className="cursor-pointer hover:opacity-45 duration-300"
                        onClick={() => (window.location.href = '/album')}
                        src={albumCover}
                        alt="Album cover"
                    />
                    <div className="opacity-0 group-hover:opacity-100 duration-300 absolute inset-x-0 bottom-0 pb-4 align-middle flex justify-center items-end text-xl text-light font-bodytext3">
                        Artist - Album Name
                    </div>
                </div>
            </div>,
        );
    });

    return (
        <div className="bg-dark h-screen w-screen font-bodytext3 font-extralight tracking-wide">
            <h1 className="pt-5 pl-2.5 pb-2.5 text-light text-3xl">FEATURED TODAY</h1>
            <div className="flex justify-center items-center justify-items-center gap-[10px] mx-4 mb-[50px]">
                <div className="w-[1200px] h-full text-light">
                    <div className="relative group">
                        <img
                            className="cursor-pointer hover:opacity-45 duration-300"
                            onClick={() => (window.location.href = '/playlist')}
                            src="https://www.the-sun.com/wp-content/uploads/sites/6/2023/10/www-instagram-com-monkeycatluna-hl-851711797.jpg"
                            alt="Playlist 1 cover"
                        />
                        <div className="opacity-0 group-hover:opacity-100 duration-300 absolute inset-x-0 bottom-0 pb-4 align-middle flex justify-center items-end text-xl text-light font-bodytext3">
                            some playlist info
                        </div>
                    </div>
                    <div className="flex justify-end font-WAVEE3 text-xl p-3 text-light">
                        <h2>playlist 1</h2>
                    </div>
                </div>

                <div className="text-light w-[1200px] h-full">
                    <div className="relative group">
                        <img
                            className="cursor-pointer hover:opacity-45 duration-300"
                            onClick={() => (window.location.href = '/playlist')}
                            src="https://i.pinimg.com/736x/a8/14/eb/a814eb4c51518adcf827f4ee64137f7c.jpg"
                            alt="Playlist 2 cover"
                        />
                        <div className="opacity-0 group-hover:opacity-100 duration-300 absolute inset-x-0 bottom-0 pb-4 align-middle flex justify-center items-end text-xl text-light font-bodytext3">
                            some playlist info
                        </div>
                    </div>
                    <div className="flex justify-end font-WAVEE3 text-xl p-3 text-light">
                        <h2>playlist 2</h2>
                    </div>
                </div>

                <div className="text-light w-[1200px] h-full">
                    <div className="relative group">
                        <img
                            className="cursor-pointer hover:opacity-45 duration-300"
                            onClick={() => (window.location.href = '/playlist')}
                            src="https://pro2-bar-s3-cdn-cf4.myportfolio.com/dbea3cc43adf643e2aac2f1cbb9ed2f0/f14d6fc4-2cea-41a2-9724-a7e5dff027e8_rw_1200.jpg?h=60e8fb45f75e1a2612c53a4f2174997c"
                            alt="Playlist 3 cover"
                        />
                        <div className="opacity-0 group-hover:opacity-100 duration-300 absolute inset-x-0 bottom-0 pb-4 align-middle flex justify-center items-end text-xl text-light font-bodytext3">
                            some playlist info
                        </div>
                    </div>
                    <div className="flex justify-end font-WAVEE3 text-xl p-3 text-light">
                        <h2>playlist 3</h2>
                    </div>
                </div>

                <div className="text-light w-[1200px] h-full">
                    <div className="relative group">
                        <img
                            className="cursor-pointer hover:opacity-45 duration-300"
                            onClick={() => (window.location.href = '/playlist')}
                            src="https://i.pinimg.com/originals/a3/bb/2f/a3bb2fd39bf2067afb7083e7176aded9.jpg"
                            alt="Playlist 4 cover"
                        />
                        <div className="opacity-0 group-hover:opacity-100 duration-300 absolute inset-x-0 bottom-0 pb-4 align-middle flex justify-center items-end text-xl text-light font-bodytext3">
                            some playlist info
                        </div>
                    </div>
                    <div className="flex justify-end font-WAVEE3 text-xl p-3 text-light">
                        <h2>playlist 4</h2>
                    </div>
                </div>
            </div>

            {/* album / posts */}
            <div className="pt-8 pb-8 bg-dark grid grid-cols-[55%_35%] gap-16 px-4">
                {/* new releases */}
                <div className="">
                    <h1 className="pl-2.5 pb-2.5 m-0 text-light text-3xl">NEW RELEASES</h1>
                    <div className="grid grid-cols-2 gap-5">{albumCoversList}</div>
                </div>

                {/* trending posts */}
                <div className=''>
                    <h1 className="flex justify-between pl-2.5 pb-2.5 m-0 text-light text-3xl">
                        RECENT POSTS
                        <img
                            className='cursor-pointer w-[40px] h-[40px] hover:w-[43px] hover:h-[43px] duration-200'
                            src='/public/seeMore-lightblue.svg'
                            onClick={() => (window.location.href = '/post-list')}
                        />
                    </h1>
                    <div className="flex top-0 pl-6 justify-left space-x-16 bg-dark columns-6 border-[1px] border-light-blue rounded-3xl">
                        <div className="posts-container pt-6 text-light font-bodytext3">
                            {postsArray.map((post, index) => (
                                <div>
                                    <Link key={index} to={`/post/${post.id}`} className="text-light">
                                        <div className="post flex justify-between">
                                            <h2 className='hover:text-light-blue'>{post.title}</h2>
                                            <p>{format.date(post.modified_at)}</p>
                                            <br></br>
                                        </div>
                                    </Link>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
