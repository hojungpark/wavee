import React from 'react';

// const request = async () => {
//     const response = await fetch("http://localhost:8080/auth/login");
//     const posts = await response.json();

//     console.log(posts)
// }

function LandingPage() {
  return (
    <div>
      <h1>WAVEE</h1>
      <button
        className='btn btn-accent'
        onClick={() => { window.location.href='http://localhost:8080/auth/login'; }}
      >
        Login with Spotify
      </button>
    </div>
  )
}

export default LandingPage;

