import React from 'react';

const User = () => {
    return (
        <div className="min-h-screen bg-dark">
            <div className="flex justify-center">
                <div className="pt-6 avatar">
                    <div className="w-60 rounded-full ring ring-light-blue ring-offset-dark ring-offset-2">
                        <img src="https://i.pinimg.com/736x/36/73/f4/3673f4920308afc95b0b3a4629f97400.jpg" />
                    </div>
                </div>
            </div>

            <div className="flex justify-center">
                <h1 className="pt-6 text-2xl tracking-wide text-light">username123</h1>
            </div>

            <div className="divider"></div>
            <div className="bg-dark columns-4">
                <div className="card bg-transparent text-light w-full">
                    <figure>
                        <img
                            className="max-w-[400px] h-[400px]"
                            src="https://www.the-sun.com/wp-content/uploads/sites/6/2023/10/www-instagram-com-monkeycatluna-hl-851711797.jpg"
                            alt="Playlist 1 cover"
                        />
                    </figure>
                    <div className="card-body">
                        <h2 className="card-title">playlist 1</h2>
                        <p>some playlist info</p>
                        <div className="card-actions justify-end">
                            <button
                                className="btn bg-dark text-light"
                                onClick={(event) => (window.location.href = '/Playlist')}
                            >
                                view
                            </button>
                        </div>
                    </div>
                </div>

                <div className="card bg-transparent text-light w-full">
                    <figure>
                        <img
                            className="max-w-[400px] h-[400px]"
                            src="https://cdn.imweb.me/upload/S2022102074691921248d5/637d3e9a2db27.jpg"
                            alt="Playlist 2 cover"
                        />
                    </figure>
                    <div className="card-body">
                        <h2 className="card-title">playlist 2</h2>
                        <p>some playlist info</p>
                        <div className="card-actions justify-end">
                            <button
                                className="btn bg-dark text-light"
                                onClick={(event) => (window.location.href = '/Playlist')}
                            >
                                view
                            </button>
                        </div>
                    </div>
                </div>

                <div className="card bg-transparent text-light w-full">
                    <figure>
                        <img
                            className="max-w-[400px] h-[400px]"
                            src="https://pro2-bar-s3-cdn-cf4.myportfolio.com/dbea3cc43adf643e2aac2f1cbb9ed2f0/f14d6fc4-2cea-41a2-9724-a7e5dff027e8_rw_1200.jpg?h=60e8fb45f75e1a2612c53a4f2174997c"
                            alt="Playlist 3 cover"
                        />
                    </figure>
                    <div className="card-body">
                        <h2 className="card-title">playlist 3</h2>
                        <p>some playlist info</p>
                        <div className="card-actions justify-end">
                            <button
                                className="btn bg-dark text-light"
                                onClick={(event) => (window.location.href = '/Playlist')}
                            >
                                view
                            </button>
                        </div>
                    </div>
                </div>

                <div className="card bg-transparent text-light w-full">
                    <figure>
                        <img
                            className="max-w-[400px] h-[400px]"
                            src="https://i.pinimg.com/originals/a3/bb/2f/a3bb2fd39bf2067afb7083e7176aded9.jpg"
                            alt="Playlist 4 cover"
                        />
                    </figure>
                    <div className="card-body">
                        <h2 className="card-title">playlist 4</h2>
                        <p>some playlist info</p>
                        <div className="card-actions justify-end">
                            <button
                                className="btn bg-dark text-light"
                                onClick={(event) => (window.location.href = '/Playlist')}
                            >
                                view
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default User;
