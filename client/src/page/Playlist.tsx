
const Playlist = () => {
    return (
        <div className="pt-6 flex flex-col w-full min-h-screen bg-dark font-bodytext3 px-2">
            <div className="grid grid-cols-[40%_60%] text-light gap-10 mx-3">
                <figure className="max-w-[500px] max-h-[550px] pb-6">
                    <img
                        src="https://www.the-sun.com/wp-content/uploads/sites/6/2023/10/www-instagram-com-monkeycatluna-hl-851711797.jpg"
                        alt="Playlist 1 cover"
                    />
                </figure>
                <div className="font-bodytext3 max-h-[470px] pt-4 pb-0">
                    <button
                        className="text-light-blue hover:font-bold hover:tracking-normal pb-2"
                        onClick={() => (window.location.href = '/user')}
                    >
                        user123
                    </button>
                    <div>
                        <h1 className="text-8xl font-bodytext3 font-extrabold pb-6">playlist 1</h1>
                        <p className="font-thin text-xl tracking-wide">some playlist info</p>
                        <div className="flex justify-start font-bodytext3">
                            <button className="align-bottom select-none font-bodytext3 tracking-wider font-medium text-center transition-all disabled:opacity-50 disabled:shadow-none disabled:pointer-events-none text-xs mt-2 py-[10px] px-[12px] mr-2 bg-gradient-to-tr from-green-400/70 to-blue-400/70 text-white hover:shadow-md hover:shadow-green-400/20 rounded-full" type="button">
                                listen on Spotify
                            </button>
                        </div>
                    </div>
                    {/* <h1 className="text-8xl font-bodytext3 font-extrabold pb-6">playlist 1</h1>
                    <p className="font-thin text-xl tracking-wide">some playlist info</p>
                    <div className="flex justify-start font-bodytext3">
                        <button className="align-middle select-none font-bodytext3 tracking-wider font-medium text-center transition-all disabled:opacity-50 disabled:shadow-none disabled:pointer-events-none text-xs mt-2 py-[10px] px-[12px] mr-2 bg-gradient-to-tr from-green-400/70 to-blue-400/70 text-white hover:shadow-md hover:shadow-green-400/20 rounded-full" type="button">
                            listen on Spotify
                        </button>
                    </div> */}
                </div>
            </div>

            <div className="divider divider-start text-light"></div>

            <div className="grid grid-cols-[60%_40%]">
                <div className="px-10 h-20">
                    <h1 className="font-bodytext3 font-bold text-2xl text-light pb-3">
                        Comments
                    </h1>
                    <div className="flex justify-start gap-3 mb-7">
                        <div className="w-10 rounded-full">
                            <img
                                alt="Tailwind CSS chat bubble component"
                                src="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg"
                            />
                        </div>
                        <input
                            type="text" id="comment"
                            className="border-b-[1px] bg-dark w-full border-light focus:outline-none text-light focus:border-light-blue focus:border-b-2 caret-light-blue"
                            placeholder="add your comment..."
                        />
                        <button className="bg-dark text-light">
                            Cancel
                        </button>
                        <button className="bg-light-blue/60 hover:bg-light-blue/40 duration-100 text-light px-2 rounded-full">
                            Comment
                        </button>
                    </div>
                    <div className="flex justify-start max-w-[500px] gap-2 pt-2">
                        <div className="w-10 rounded-full">
                            <img
                                alt="Tailwind CSS chat bubble component"
                                src="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg"
                            />
                        </div>
                        <div className="text-white w-[500px] border-[1px] border-white/70 rounded-full px-2 py-1">
                        I actually like this playlist
                        </div>
                    </div>

                    <div className="flex justify-start max-w-[500px] gap-2 pt-5">
                        <div className="w-10 rounded-full">
                            <img
                                alt="Tailwind CSS chat bubble component"
                                src="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg"
                            />
                        </div>
                        <div className="text-white w-[500px] border-[1px] border-white/70 rounded-full px-2 py-1">
                            lets gooooooooo
                        </div>
                    </div>

                    <div className="flex justify-start max-w-[500px] gap-2 pt-5">
                        <div className="w-10 rounded-full">
                            <img
                                alt="Tailwind CSS chat bubble component"
                                src="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg"
                            />
                        </div>
                        <div className="text-white w-[500px] border-[1px] border-white/70 rounded-full px-2 py-1">
                            meh
                        </div>
                    </div>
                </div>

                <div className="pb-3 mr-5">
                    <h1 className="font-bodytext3 text-2xl font-bold text-light pb-3">
                        Tracks
                    </h1>
                    <div className="font-bodytext3 font-light text-light/70">
                        <div className="grid grid-cols-[10%_90%] items-start mb-3">
                            <div className="max-w-12 max-h-12 pr-1">
                                <img
                                    src="https://i.scdn.co/image/ab67616d0000b273583ea833d682bb284c5905b4"
                                    alt="Playlist cover 1"
                                />
                            </div>
                            <div className="flex items-start max-w-[400px] justify-items-center justify-between">
                                <div>
                                    <h1 className="font-bold text-light">blue</h1>
                                    <h2>Kamal.</h2>
                                </div>
                                <div>
                                    blue - Single
                                </div>
                                <div>
                                    3:05
                                </div>
                            </div>
                        </div>
                        <div className="grid grid-cols-[10%_90%] items-start mb-2">
                            <div className="max-w-12 max-h-12 pr-1">
                                <img
                                    src="https://i.scdn.co/image/ab67616d0000b273583ea833d682bb284c5905b4"
                                    alt="Playlist cover 1"
                                />
                            </div>
                            <div className="flex items-start max-w-[400px] justify-items-center justify-between">
                                <div>
                                    <h1 className="font-bold text-light">blue</h1>
                                    <h2>Kamal.</h2>
                                </div>
                                <div>
                                    blue - Single
                                </div>
                                <div>
                                    3:05
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Playlist;
