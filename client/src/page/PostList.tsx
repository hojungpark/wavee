import axios from 'axios';
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getEnv } from '../utils/env';
import { response } from '../utils/responses';
import { useNavigate } from 'react-router-dom';

const PostList = () => {
    const navigate = useNavigate();
    const [posts, setPosts] = useState<response.Post[]>([]);
    const url = getEnv('DEV') ? `http://localhost:${getEnv('VITE_PORT')}` : getEnv('VITE_URL');
    useEffect(() => {
        axios
            .get<response.Post[]>(`${url}/api/community/posts`)
            .then((response) => {
                setPosts(response.data); // Set posts with response data
            })
            .catch((error) => console.error('Error fetching posts:', error));
    }, []);

    let data = Array.from(posts);

    return (
        <div className="bg-dark h-screen font-bodytext3 font-extralight tracking-wide">
            <h1 className="font-WAVEE3 pt-3 pl-2.5 pb-2.5 text-light font-bold text-3xl tracking-wide">Posts</h1>

            <div className="flex pl-6 justify-left space-x-16 bg-dark columns-6 h-56">
                <div className="posts-container">
                    {data.map((post, index) => (
                        <div>
                            <Link key={index} to={`/post/${post.id}`} className="link link-hover text-light">
                                <div className="post">
                                    <h2>{post.title}</h2>
                                </div>
                            </Link>
                        </div>
                    ))}
                </div>
            </div>
            <button className="link link-hover text-light" onClick={() => navigate('/post-form')}>
                Create Post
            </button>
        </div>
    );
};

export default PostList;
