import { useEffect, useState } from 'react';
import axios from 'axios';
import { useParams } from 'react-router-dom';
import { getEnv } from '../utils/env';
import { response } from '../utils/responses';
import { format } from '../utils/format';

const Post = () => {
    const { id } = useParams<{ id: string }>();
    const [post, setPost] = useState<response.Post | null>(null);
    const [comments, setComments] = useState<response.Comments[]>([]);

    const url = getEnv('DEV') ? `http://localhost:${getEnv('VITE_PORT')}` : getEnv('VITE_URL');

    useEffect(() => {
        axios
            .get<response.Post>(`${url}/api/community/posts/${id}`)
            .then((response) => {
                setPost(response.data);
            })
            .catch((error) => {
                console.error('Error fetching post:', error);
            });
        axios
            .get<response.Comments[]>(`${url}/api/community/posts/${id}/comments`)
            .then((response) => {
                setComments(response.data);
            })
            .catch((error) => {
                console.error('Error fetching post:', error);
            });
    }, [id]);

    if (!post) return <div>Post not found</div>;

    return (
        <div className="pt-10 min-h-screen bg-dark">
            <h1 className="pt-4 pl-72 text-light-blue text-6xl">Post Title: {post.title}</h1>
            <h3 className="pt-4 pl-72 text-light">Spotify User ID: {post.spotify_user_id}</h3>
            <div className="pt-6 pl-80 text-light">
                <p>{post.body}</p>
            </div>

            <div className="divider"></div>

            <div className="flex justify-center space-x-16">
                <button className="btn bg-dark text-light">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        className="h-6 w-6"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke="currentColor"
                    >
                        <path
                            strokeLinecap="round"
                            strokeLinejoin="round"
                            strokeWidth="2"
                            d="M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z"
                        />
                    </svg>
                    123
                </button>
                <button className="btn bg-dark text-light">
                    <svg xmlns="http://www.w3.org/2000/svg" className="fill-light" width="32" height="32" id="share">
                        <path d="M19.065 30.114 30.963 2.456a1.035 1.035 0 0 0-1.314-1.314L2.145 12.931c-.854.363-1.222 1.305-1.157 2.104.062.744.483 1.568 1.385 1.766l11.088 1.844 1.986 11.357c1.063 1.192 2.587 1.192 3.618.112zm6.598-25.088-11.684 11.68-10.876-1.814c.038-.104.068-.18.104-.242l22.456-9.624zm1.412 1.414-9.606 22.42c-.091.059-.185.025-.281 0l-1.789-10.736L27.075 6.44z"></path>
                    </svg>
                    123
                </button>
            </div>

            <div className="divider"></div>
            <div className="pl-72 grid h-20 flex-grow card bg-transparent rounded-box">
                <h1 className="text-light">comments</h1>
                <div className="chat chat-start">
                    {comments.map((comment) => (
                        <div>
                            <div className="chat-image avatar">
                                <div className="w-10 rounded-full">
                                    <img
                                        alt="Tailwind CSS chat bubble component"
                                        src="https://daisyui.com/images/stock/photo-1534528741775-53994a69daeb.jpg"
                                    />
                                </div>
                            </div>
                            <div className="chat-bubble chat-bubble-primary text-white">{comment.body}</div>
                            <div className="chat-bubble chat-bubble-primary text-white">
                                {format.date(comment.modified_at)}
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        </div>
    );
};

export default Post;
