import { useState } from 'react';
import { BrowserRouter, Routes, Route, Link, NavLink } from 'react-router-dom';
import Home from './page/Home';
import Playlist from './page/Playlist';
import User from './page/User';
import Post from './page/Post';
// import LandingPage from './page/LandingPage';
import PostForm from './page/PostForm';
import Album from './page/Album';
import PostList from './page/PostList';

function Router() {
    const [isOpen, setIsOpen] = useState(false);

    const toggleDropdown = () => {
        setIsOpen(!isOpen);
    };

    const closeDropdown = () => {
        setIsOpen(false);
    };

    return (
        <BrowserRouter>
            {/* navigation bar */}
            <div className='flex justify-between bg-dark pt-5 pb-3 relative'>

                <div className="group relative w-[10px] flex items-center justify-between">
                    <a className="my-2 py-2 text-base font-medium text-white lg:mx-4"></a>
                    <span>
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            stroke-width="2"
                            stroke="white"
                            className="w-6 h-6"
                        >
                            <path
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
                            />
                        </svg>
                    </span>
                    <div className="invisible origin-top-left absolute top-0 mt-7 w-44 rounded-lg shadow-lg bg-dark/80 group-hover:visible tracking-wide">
                        <ul
                            tabIndex={0}
                            className="menu menu-sm dropdown-content mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52 text-light font-bodytext3 tracking-wide"
                        >
                            <nav>
                                <NavLink
                                    className={({ isActive }) => 'nav-link' + (isActive ? ' click' : '')}
                                    to={'/Playlist'}
                                    onClick={closeDropdown}
                                >
                                    PLAYLIST
                                </NavLink>
                            </nav>
                            <nav>
                                <NavLink
                                    className={({ isActive }) => 'nav-link' + (isActive ? ' click' : '')}
                                    to={'/User'}
                                    onClick={closeDropdown}
                                >
                                    USER
                                </NavLink>
                            </nav>
                            <nav>
                                <NavLink
                                    className={({ isActive }) => 'nav-link' + (isActive ? ' click' : '')}
                                    to={'/post-list'}
                                >
                                    POST
                                </NavLink>
                            </nav>
                        </ul>
                    </div>
                </div>

                <div>
                    <a
                        className="mt-2 lg:absolute max-lg:top-4 max-lg:left-10 max-sm:left-4 lg:top-2/4 lg:left-2/4 lg:-translate-x-1/2 lg:-translate-y-1/2 font-medium text-5xl text-white font-WAVEE3 hover:blur-sm duration-300"
                        href="/"
                        onClick={() => (window.location.href = '/')}
                    >
                        WAVEE
                    </a>
                </div>

                <div className='pr-5'>
                <button className="flex align-middle gap-2 select-none font-bodytext3 tracking-wider font-medium text-center transition-all disabled:opacity-50 disabled:shadow-none disabled:pointer-events-none text-sm mt-2 py-[10px] px-[12px] mr-2 bg-gradient-to-tr from-green-400/70 to-blue-400/70 text-white hover:shadow-md hover:shadow-green-400/20 rounded-full" type="button">
                    <img className='w-[20px] h-[20px]' src='/public/Spotify_Icon_RGB_White.png' />
                    Login with Spotify
                </button>
                </div>
            </div>

            <Routes>
                {/* <Route path='/' element={<LandingPage />} /> */}
                <Route path="/" element={<Home />} />
                <Route path="/playlist" element={<Playlist />} />
                <Route path="/user" element={<User />} />
                <Route path="/post/:id" element={<Post />} />
                <Route path="/post-form" element={<PostForm />} />
                <Route path="/album" element={<Album />} />
                <Route path="/post-list" element={<PostList />} />
            </Routes>
        </BrowserRouter>
    );
}

export default Router;
