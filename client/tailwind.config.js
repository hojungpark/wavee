/** @type {import('tailwindcss').Config} */
const withMT = require('@material-tailwind/html/utils/withMT');

module.exports = withMT({
    content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
    theme: {
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            white: '#ffffff',
            dark: '#323232',
            light: '#E4EDF3',
            'light-blue': '#8DD6ED',
        },
        extend: {
            fontFamily: {
                WAVEE: ['Rubik Glitch Pop', 'system-ui'],
                WAVEE1: ['Black Han Sans', 'sans-serif'],
                WAVEE2: ['Racing Sans One', 'sans-serif'],
                WAVEE3: ['Julius Sans One', 'sans-serif'],

                bodytext: ['Quicksand', 'sans-serif'],
                bodytext2: ['Cutive Mono', 'monospace'],
                bodytext3: ['Manrope', 'sans-serif'],
            },
        },
    },

    plugins: [],
});

// export default {
//     content: ['./index.html', './src/**/*.{js,ts,jsx,tsx}'],
//     theme: {
//         colors: {
//             transparent: 'transparent',
//             current: 'currentColor',
//             white: '#ffffff',
//             dark: '#323232',
//             light: '#E4EDF3',
//             'light-blue': '#8DD6ED',
//         },
//         extend: {
//             fontFamily: {
//                 WAVEE: ['Rubik Glitch Pop', 'system-ui'],
//                 WAVEE1: ['Black Han Sans', 'sans-serif'],
//                 WAVEE2: ['Racing Sans One', 'sans-serif'],
//                 WAVEE3: ['Julius Sans One', 'sans-serif'],

//                 bodytext: ['Quicksand', 'sans-serif'],
//                 bodytext2: ['Cutive Mono', 'monospace'],
//                 bodytext3: ['Manrope', 'sans-serif'],
//             },
//         },
//     },

//     plugins: [],
// };
