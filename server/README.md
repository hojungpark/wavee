# Server

## [Database Structure](https://dbdiagram.io/d/CMPT-372-Group-25-Database-Structure-65fc999eae072629cea6bac8)

![Database Structure](Wavee-DB.png 'Database Structure')

## Cloud Development

### Accessing Database

-   In [Google Cloud Platform](https://console.cloud.google.com/welcome?authuser=1&hl=en&project=wavee-417322), activate cloud shell by clicking on the terminal icon in the top right corner.

-   Once you are connected to Cloud Shell, connect to Cloud SQL with this command:

```gcloud
gcloud sql connect wavee-db-instance -u postgres -d wavee-db
```

## Local Development

### Create Database

-   Create a PostgreSQL database in your local environment

```BASH
createdb -h localhost -p <PORT> -U <YOUR_USERNAME> <DATABASE_NAME>
```

### Setup Local Environment

-   Fill the following in the [.env](/server/.env) file accordingly

```.env
DB_LOCAL_USER=<YOUR_LOCAL_USER>
DB_LOCAL_PASSWORD=<YOUR_LOCAL_PASSWORD>
DB_LOCAL_DATABASE=<YOUR_LOCAL_DATABASE>
DB_LOCAL_HOST=<YOUR_LOCAL_HOST>
DB_LOCAL_PORT=<YOUR_LOCAL_PORT>
```

-   Running `npm run dev` will automatically set `NODE_ENV` to `development`. It will use your local PostgreSQL database according to your environment variables

### Create database

-   Connect to PostgreSQL with the following command within the `server` directory.

```Bash
    psql -h localhost -p <port> -U <username>
```

-   Create database by using the following command.

```PostgreSQL
CREATE DATABASE "wavee-db";
```

### Creating Tables

-   Once you created the database, you can then connect to it and create tables by using the `server/Wavee-DB.sql` file with the following command.

```SQL
\c "wavee-db";
\i Wavee-DB.sql;
```
