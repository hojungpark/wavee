import { NextFunction, Request, Response } from 'express';
import { SpotifyApi } from '@spotify/web-api-ts-sdk';
import * as spotify from '../api/spotify';

export const getCurrentUserProfile = async (
    req: Request<{}, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const userProfile = await spotify.getCurrentUserProfile(req.sdk!);
        res.status(200).json(userProfile);
    } catch (error) {
        console.error('getCurrentUserProfile Error:', error);
        res.status(500).send('getCurrentUserProfile Error');
    }
};

export const getCurrentUserPlaylists = async (
    req: Request<{}, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const currentUserPlaylists = await spotify.getCurrentUserPlaylists(req.sdk!);
        res.status(200).json(currentUserPlaylists);
    } catch (error) {
        console.error('getCurrentUserPlaylists Error:', error);
        res.status(500).send('getCurrentUserPlaylists Error');
    }
};

export const getCurrentUserSavedAlbums = async (
    req: Request<{}, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        if (!req.sdk) {
            return res.status(400).send('Sporify API is missing');
        }
        const currentUserSavedAlbums = await spotify.getCurrentUserSavedAlbums(req.sdk!);
        res.status(200).json(currentUserSavedAlbums);
    } catch (error) {
        console.error('getCurrentUserSavedAlbums Error:', error);
        res.status(500).send('getCurrentUserSavedAlbums Error');
    }
};

export const getCurrentUserTopArtists = async (
    req: Request<{}, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const currentUserTopArtists = await spotify.getCurrentUserTopArtists(req.sdk!);
        res.status(200).json(currentUserTopArtists);
    } catch (error) {
        console.error('getCurrentUserTopArtists Error:', error);
        res.status(500).send('getCurrentUserTopArtists Error');
    }
};

export const getCurrentUserTopTracks = async (
    req: Request<{}, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const currentUserTopTracks = await spotify.getCurrentUserTopTracks(req.sdk!);
        res.status(200).json(currentUserTopTracks);
    } catch (error) {
        console.error('getCurrentUserTopTracks Error:', error);
        res.status(500).send('getCurrentUserTopTracks Error');
    }
};

export const getFeaturedPlaylists = async (
    req: Request<{}, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const featuredPlaylists = await spotify.getFeaturedPlaylists(req.sdk!);
        res.status(200).json(featuredPlaylists);
    } catch (error) {
        console.error('getFeaturedPlaylists Error:', error);
        res.status(500).send('getFeaturedPlaylists Error');
    }
};

export const getNewReleases = async (req: Request<{}, {}, { sdk: SpotifyApi }>, res: Response, next: NextFunction) => {
    try {
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const newReleases = await spotify.getNewReleases(req.sdk!);
        res.status(200).json(newReleases);
    } catch (error) {
        console.error('getNewReleases Error:', error);
        res.status(500).send('getNewReleases Error');
    }
};

export const getUserProfile = async (
    req: Request<{ userId: string }, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        const {
            params: { userId },
        } = req;

        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const userProfile = await spotify.getUserProfile(req.sdk!, userId);
        res.status(200).json(userProfile);
    } catch (error) {
        console.error('getUserProfile Error:', error);
        res.status(500).send('getUserProfile Error');
    }
};

export const getAlbum = async (
    req: Request<{ albumId: string }, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        const {
            params: { albumId },
        } = req;
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const album = await spotify.getAlbum(req.sdk!, albumId);
        res.status(200).json(album);
    } catch (error) {
        console.error('getAlbum Error:', error);
        res.status(500).send('getAlbum Error');
    }
};

export const getAlbumTracks = async (
    req: Request<{ albumId: string }, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        const {
            params: { albumId },
        } = req;
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const albumTracks = await spotify.getAlbumTracks(req.sdk!, albumId);
        res.status(200).json(albumTracks);
    } catch (error) {
        console.error('getAlbumTracks Error:', error);
        res.status(500).send('getAlbumTracks Error');
    }
};

export const getPlaylist = async (
    req: Request<{ playlistId: string }, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        const {
            params: { playlistId },
        } = req;
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const playlist = await spotify.getPlaylist(req.sdk!, playlistId);
        res.status(200).json(playlist);
    } catch (error) {
        console.error('getPlaylist Error:', error);
        res.status(500).send('getPlaylist Error');
    }
};

export const getPlaylistTracks = async (
    req: Request<{ playlistId: string }, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        const {
            params: { playlistId },
        } = req;
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const playlistTracks = await spotify.getPlaylistTracks(req.sdk!, playlistId);
        res.status(200).json(playlistTracks);
    } catch (error) {
        console.error('getPlaylistTracks Error:', error);
        res.status(500).send('getPlaylistTracks Error');
    }
};

export const getTrack = async (
    req: Request<{ trackId: string }, {}, { sdk: SpotifyApi }>,
    res: Response,
    next: NextFunction,
) => {
    try {
        const {
            params: { trackId },
        } = req;
        if (!req.sdk) {
            return res.status(400).send('Spotify API is missing');
        }
        const track = await spotify.getTrack(req.sdk!, trackId);
        res.status(200).json(track);
    } catch (error) {
        console.error('getTrack Error:', error);
        res.status(500).send('getTrack Error');
    }
};
