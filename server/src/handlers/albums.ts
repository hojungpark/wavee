import { Request, Response } from 'express';
import * as db from '../api/db';

export const getAlbumComments = async (req: Request, res: Response) => {
    try {
        const comments = await db.getAlbumComments(req.pool!);
        res.status(200).send(comments);
    } catch (error) {
        console.error('handler.getAlbumComments error:', error);
        res.status(500).send(`handler.getAlbumComments error: ${error}`);
    }
};

export const getAlbumCommentsByAid = async (req: Request<{ aid: string }>, res: Response) => {
    try {
        const {
            params: { aid },
        } = req;
        const comments = await db.getAlbumCommentsByAid(req.pool!, aid);
        if (comments == null) {
            return res.sendStatus(404);
        }
        res.status(200).send(comments);
    } catch (error) {
        console.error('handler.getAlbumCommentsByAid error:', error);
        res.status(500).send(`handler.getAlbumCommentsByAid error: ${error}`);
    }
};

export const createAlbumComment = async (req: Request, res: Response) => {
    try {
        const { body } = req;
        const comment = await db.createAlbumComment(req.pool!, body.album_id, body.body, body.spotify_user_id);
        res.status(201).send(comment);
    } catch (error) {
        console.error('handler.createAlbumComment error:', error);
        res.status(500).send(`handler.createAlbumComment error: ${error}`);
    }
};

export const getAlbumCommentByCid = async (req: Request, res: Response) => {
    try {
        const {
            params: { cid },
        } = req;
        const comment = await db.getAlbumCommentByCid(req.pool!, cid);
        if (comment == null) {
            return res.sendStatus(404);
        }
        res.status(200).send(comment);
    } catch (error) {
        console.error('handler.getAlbumCommentByCid error:', error);
        res.status(500).send(`handler.getAlbumCommentByCid error: ${error}`);
    }
};

export const editAlbumComment = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
            body,
        } = req;
        const comment = await db.editAlbumComment(req.pool!, cid, body.body);
        res.status(200).send(comment);
    } catch (error) {
        console.error('handler.editAlbumComment error:', error);
        res.status(304).send(`handler.editAlbumComment error: ${error}`);
    }
};

export const deleteAlbumComment = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
        } = req;
        const comment = await db.deleteAlbumComment(req.pool!, cid);
        res.status(204).send(comment);
    } catch (error) {
        console.error('handler.deleteAlbumComment error:', error);
        res.status(500).send('handler.deleteAlbumComment error');
    }
};
