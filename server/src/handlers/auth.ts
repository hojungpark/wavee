import { Request, Response } from 'express';
import { CallbackQueryParams } from '../types/query';
import { AuthParams } from '../types/auth';
import { generateCodeChallenge, generateAuthUrl } from '../utils/auth';
import { requestAccessToken, requestUserProfile } from '../api/spotify';

declare module 'express-session' {
    export interface SessionData {
        state: string;
        codeVerifier: string;
        accessTokenData: any;
    }
}

export interface AccessTokenResponse {
    access_token: string;
    token_type: string;
    scope: string;
    expires_in: number;
    refresh_token: string;
}

export const login = (req: Request<unknown, AuthParams>, res: Response) => {
    // Get authParams extended from middleware 'getAuthParams'.
    if (!req.clientId) {
        return res.status(400).send('clientId not extended in req');
    }
    if (!req.redirectUri) {
        return res.status(400).send('redirectUri not extended in req');
    }
    if (!req.scopes) {
        return res.status(400).send('scopes not extended in req');
    }

    const { clientId, redirectUri, scopes } = req;
    const { codeVerifier, codeChallenge, state } = generateCodeChallenge();

    // Store the codeVerifier and state in the session
    req.session.codeVerifier = codeVerifier;
    req.session.state = state;

    const params = {
        clientId: clientId!,
        redirectUri: redirectUri!,
        scopes: scopes!,
        codeChallenge: codeChallenge,
        state: state,
    };

    const authUrl = generateAuthUrl(params);
    res.status(200).redirect(authUrl);
};

export const callback = async (req: Request<unknown, unknown, unknown, CallbackQueryParams>, res: Response) => {
    if (!req.clientId) {
        return res.status(400).send('clientId not extended in req');
    }
    if (!req.redirectUri) {
        return res.status(400).send('redirectUri not extended in req');
    }
    const { clientId, redirectUri } = req;

    if (!req.query.code) {
        return res.status(400).send('Authorization code is missing');
    }
    if (!req.query.state) {
        return res.status(403).send('State is missing');
    }
    const { code, state } = req.query;

    if (req.session.state !== state) {
        return res.status(403).send('State mismatch');
    }
    if (!req.session.codeVerifier) {
        return res.status(403).send('Code verifier is missing');
    }
    const { codeVerifier } = req.session;

    const params = new URLSearchParams({
        grant_type: 'authorization_code',
        code: code,
        client_id: clientId!,
        redirect_uri: redirectUri!,
        code_verifier: codeVerifier,
    });

    const accessTokenData = await requestAccessToken(params);
    req.session.accessTokenData = accessTokenData;
    req.session.save();

    const userProfileResponse = await requestUserProfile(accessTokenData.access_token);
    res.status(200);
        // .cookie('accessTokenData', JSON.stringify(accessTokenData), { path: '/callback' })
        // .send(userProfileResponse);

    res.cookie('accessTokenData', JSON.stringify(accessTokenData), { httpOnly: true, secure: true, sameSite: 'none', path: '/' });
    res.redirect('https://wavee-frontend-brsnvqlvoq-uw.a.run.app/');
};

export const logout = (req: Request, res: Response) => {
    req.session.destroy((error) => {
        res.redirect('/');
    });
};
