import { Request, Response } from 'express';
import * as db from '../api/db';

export const getPlaylistComments = async (req: Request, res: Response) => {
    try {
        const comments = await db.getPlaylistComments(req.pool!);
        res.status(200).send(comments);
    } catch (error) {
        console.error('handler.getPlaylistComments error:', error);
        res.status(500).send(`handler.getPlaylistComments error: ${error}`);
    }
};

export const getPlaylistCommentsByPid = async (req: Request<{ pid: string }>, res: Response) => {
    try {
        const {
            params: { pid },
        } = req;
        const comments = await db.getPlaylistCommentsByPid(req.pool!, pid);
        res.status(200).send(comments);
    } catch (error) {
        console.error('handler.getPlaylistCommentsByAid error:', error);
        res.status(500).send(`handler.getPlaylistCommentsByAid error: ${error}`);
    }
};

export const createPlaylistComment = async (req: Request, res: Response) => {
    try {
        const { body } = req;
        const comment = await db.createPlaylistComment(req.pool!, body.playlist_id, body.body, body.spotify_user_id);
        res.status(201).send(comment);
    } catch (error) {
        console.error('handler.createPlaylistComment error:', error);
        res.status(500).send(`handler.createPlaylistComment error: ${error}`);
    }
};

export const getPlaylistCommentByCid = async (req: Request, res: Response) => {
    try {
        const {
            params: { cid },
        } = req;
        const comment = await db.getPlaylistCommentByCid(req.pool!, cid);
        if (comment == null) {
            return res.sendStatus(404);
        }
        res.status(200).send(comment);
    } catch (error) {
        console.error('handler.getPlaylistCommentByCid error:', error);
        res.status(500).send(`handler.getPlaylistCommentByCid error: ${error}`);
    }
};

export const editPlaylistComment = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
            body,
        } = req;
        const comment = await db.editPlaylistComment(req.pool!, cid, body.body);
        res.status(200).send(comment);
    } catch (error) {
        console.error('handler.editPlaylistComment error:', error);
        res.status(304).send(`handler.editPlaylistComment error: ${error}`);
    }
};

export const deletePlaylistComment = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
        } = req;
        const comment = await db.deletePlaylistComment(req.pool!, cid);
        res.status(204).send(comment);
    } catch (error) {
        console.error('handler.deletePlaylistComment error:', error);
        res.status(500).send('handler.deletePlaylistComment error');
    }
};
