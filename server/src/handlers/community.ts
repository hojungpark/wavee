import { Request, Response } from 'express';
import * as db from '../api/db';

export const getPosts = async (req: Request, res: Response) => {
    try {
        const posts = await db.getPosts(req.pool!);
        res.status(200).send(posts);
    } catch (error) {
        console.error('handler.getPosts error:', error);
        res.status(500).send(`handler.getPosts error: ${error}`);
    }
};

export const createPost = async (req: Request, res: Response) => {
    try {
        const { body } = req;
        const post = await db.createPost(req.pool!, body.title, body.body, body.spotify_user_id);
        res.status(201).send(post);
    } catch (error) {
        console.error('handler.createPost error:', error);
        res.status(500).send(`handler.createPost error: ${error}`);
    }
};

export const getPostById = async (req: Request<{ pid: string }>, res: Response) => {
    try {
        const {
            params: { pid },
        } = req;
        const post = await db.getPostById(req.pool!, pid);
        if (post == null) {
            return res.sendStatus(404);
        }
        res.status(200).send(post);
    } catch (error) {
        console.error('handler.getPostById error:', error);
        res.status(500).send(`handler.getPostById error: ${error}`);
    }
};

export const editPost = async (req: Request<{ pid: string }>, res: Response) => {
    try {
        const {
            params: { pid },
            body,
        } = req;
        const post = await db.editPost(req.pool!, pid, body.title, body.body);
        res.status(200).send(post);
    } catch (error) {
        console.error('handler.editPost error:', error);
        res.status(304).send(`handler.editPost error: ${error}`);
    }
};

export const deletePost = async (req: Request<{ pid: string }>, res: Response) => {
    try {
        const {
            params: { pid },
        } = req;
        const post = await db.deletePost(req.pool!, pid);
        res.status(204).send(post);
    } catch (error) {
        console.error('handler.deletePost error:', error);
        res.status(500).send('handler.deletePost error');
    }
};

export const getCommentsByPid = async (req: Request<{ pid: string }>, res: Response) => {
    try {
        const {
            params: { pid },
        } = req;
        const comments = await db.getCommentsByPid(req.pool!, pid);
        res.status(200).send(comments);
    } catch (error) {
        console.error('handler.getComments error:', error);
        res.status(500).send(`handler.getComments error: ${error}`);
    }
};

export const createComment = async (req: Request, res: Response) => {
    try {
        const { body } = req;
        const post = await db.createComment(req.pool!, body.post_id, body.body, body.spotify_user_id);
        res.status(201).send(post);
    } catch (error) {
        console.error('handler.createComment error:', error);
        res.status(500).send(`handler.createComment error: ${error}`);
    }
};

export const getComments = async (req: Request, res: Response) => {
    try {
        const comments = await db.getComments(req.pool!);
        res.status(200).send(comments);
    } catch (error) {
        console.error('handler.getComments error:', error);
        res.status(500).send(`handler.getComments error: ${error}`);
    }
};

export const getCommentById = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
        } = req;
        const comment = await db.getCommentById(req.pool!, cid);
        if (comment == null) {
            return res.sendStatus(404);
        }
        res.status(200).send(comment);
    } catch (error) {
        console.error('handler.getCommentById error:', error);
        res.status(500).send(`handler.getCommentById error: ${error}`);
    }
};

export const editComment = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
            body,
        } = req;
        const post = await db.editComment(req.pool!, cid, body.body);
        res.status(200).send(post);
    } catch (error) {
        console.error('handler.editComment error:', error);
        res.status(304).send(`handler.editComment error: ${error}`);
    }
};

export const deleteComment = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
        } = req;
        const post = await db.deleteComment(req.pool!, cid);
        res.status(204).send(post);
    } catch (error) {
        console.error('handler.deleteComment error:', error);
        res.status(500).send('handler.deleteComment error');
    }
};

export const getRepliesByCid = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
        } = req;
        const comments = await db.getRepliesByCid(req.pool!, cid);
        res.status(200).send(comments);
    } catch (error) {
        console.error('handler.getComments error:', error);
        res.status(500).send(`handler.getComments error: ${error}`);
    }
};

export const createReply = async (req: Request<{ cid: string }>, res: Response) => {
    try {
        const {
            params: { cid },
            body,
        } = req;
        const post = await db.createReply(req.pool!, cid, body.body, body.spotify_user_id);
        res.status(201).send(post);
    } catch (error) {
        console.error('handler.createReply error:', error);
        res.status(500).send(`handler.createReply error: ${error}`);
    }
};

export const getReplies = async (req: Request, res: Response) => {
    try {
        const replies = await db.getReplies(req.pool!);
        res.status(200).send(replies);
    } catch (error) {
        console.error('handler.getReplies error:', error);
        res.status(500).send(`handler.getReplies error: ${error}`);
    }
};

export const getReplyById = async (req: Request<{ rid: string }>, res: Response) => {
    try {
        const {
            params: { rid },
        } = req;
        const comment = await db.getReplyById(req.pool!, rid);
        if (comment == null) {
            return res.sendStatus(404);
        }
        res.status(200).send(comment);
    } catch (error) {
        console.error('handler.getReplyById error:', error);
        res.status(500).send(`handler.getReplyById error: ${error}`);
    }
};

export const editReply = async (req: Request<{ rid: string }>, res: Response) => {
    try {
        const {
            params: { rid },
            body,
        } = req;
        const post = await db.editReply(req.pool!, rid, body.body);
        res.status(200).send(post);
    } catch (error) {
        console.error('handler.editReply error:', error);
        res.status(304).send(`handler.editReply error: ${error}`);
    }
};

export const deleteReply = async (req: Request<{ rid: string }>, res: Response) => {
    try {
        const {
            params: { rid },
        } = req;
        const post = await db.deleteReply(req.pool!, rid);
        res.status(204).send(post);
    } catch (error) {
        console.error('handler.deleteReply error:', error);
        res.status(500).send('handler.deleteReply error');
    }
};
