export interface AuthParams {
    clientId: string;
    redirectUri: string;
    scopes: string[];
}

export interface AuthUrlParams extends AuthParams {
    codeChallenge: string;
    state: string;
}
