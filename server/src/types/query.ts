export interface CallbackQueryParams {
    code?: string;
    state?: string;
}
