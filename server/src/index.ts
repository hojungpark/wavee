import 'dotenv/config';
import express, { Request, Response } from 'express';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import authRouter from './routes/auth';
import spotifyRouter from './routes/spotify';
import communityRouter from './routes/community';
import playlistsRouter from './routes/playlists';
import albumsRouter from './routes/albums';
import { getEnv } from './utils/env';
import cors from 'cors';

const app = express();
app.use(
    session({
        secret: 'secret',
        resave: false,
        saveUninitialized: false,
        cookie: { path: '/', secure: false, maxAge: 1000 * 60 * 60 },
    }),
);

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/auth', authRouter);
app.use('/api/spotify', spotifyRouter);
app.use('/api/community', communityRouter);
app.use('/api/playlists', playlistsRouter);
app.use('/api/albums', albumsRouter);

app.get('/', (req: Request, res: Response) => {
    res.json({ message: 'Welcome to Wavee Server' });
});

const port = getEnv('PORT');
app.listen(port, () => {
    console.log(`App listening at http://localhost:${port} in ${getEnv('NODE_ENV')}`);
});
