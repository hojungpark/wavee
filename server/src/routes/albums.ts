import { Router } from 'express';
import { dbConnectSQL } from '../utils/middlewares';
import * as handler from '../handlers/albums';

const router = Router();
router.use(dbConnectSQL);

router.get('/comments', handler.getAlbumComments);
router.post('/comments', handler.createAlbumComment);
router.get('/:aid/comments', handler.getAlbumCommentsByAid);
router.get('/comments/:cid', handler.getAlbumCommentByCid);
router.put('/comments/:cid', handler.editAlbumComment);
router.delete('/comments/:cid', handler.deleteAlbumComment);

export default router;
