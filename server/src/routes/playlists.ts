import { Router } from 'express';
import { dbConnectSQL } from '../utils/middlewares';
import * as handler from '../handlers/playlists';

const router = Router();
router.use(dbConnectSQL);

router.get('/comments', handler.getPlaylistComments);
router.post('/comments', handler.createPlaylistComment);
router.get('/:pid/comments', handler.getPlaylistCommentsByPid);
router.get('/comments/:cid', handler.getPlaylistCommentByCid);
router.put('/comments/:cid', handler.editPlaylistComment);
router.delete('/comments/:cid', handler.deletePlaylistComment);

export default router;
