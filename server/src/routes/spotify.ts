import { Router } from 'express';
import session from 'express-session';
import { getAuthParams, requestSpotifySDK } from '../utils/middlewares';
import * as handler from '../handlers/spotify';

const router = Router();
router.use(
    session({
        secret: 'secret',
        resave: false,
        saveUninitialized: true,
        cookie: { secure: false, httpOnly: false },
    }),
);
router.use(getAuthParams);
router.use(requestSpotifySDK);

router.get('/me', handler.getCurrentUserProfile);
router.get('/me/playlists', handler.getCurrentUserPlaylists);
router.get('/me/albums', handler.getCurrentUserSavedAlbums);
router.get('/me/top-artists', handler.getCurrentUserTopArtists);
router.get('/me/top-tracks', handler.getCurrentUserTopTracks);
router.get('/user/:userId', handler.getUserProfile);
router.get('/album/:albumId', handler.getAlbum);
router.get('/album/:albumId/tracks', handler.getAlbumTracks);
router.get('/playlist/:playlistId', handler.getPlaylist);
router.get('/playlist/:playlistId/tracks', handler.getPlaylistTracks);
router.get('/track/:trackId', handler.getTrack);
router.get('/featured-playlists', handler.getFeaturedPlaylists);
router.get('/new-releases', handler.getNewReleases);

/* TODO: Implement these if we have time...
router.get('/artist/:artistId', getArtist);
router.get('/artist/:artistId/albums', getArtistAlbums);
router.get('/artist/:artistId/top-tracks', getArtistTopTracks);
router.get('/artist/:artistId/related-artists', getArtistRelatedArtists);

router.get('/search/playlist/:query', searchPlaylists);
router.get('/search/album/:query', searchAlbums);
router.get('/search/track/:query', searchTracks);
router.get('/search/artist/:query', searchArtists);
*/

export default router;
