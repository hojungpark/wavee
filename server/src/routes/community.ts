import { Router } from 'express';
import { dbConnectSQL } from '../utils/middlewares';
import * as handler from '../handlers/community';

const router = Router();
router.use(dbConnectSQL);

router.get('/posts', handler.getPosts);
router.post('/posts', handler.createPost);
router.get('/posts/:pid', handler.getPostById);
router.put('/posts/:pid', handler.editPost);
router.delete('/posts/:pid', handler.deletePost);

router.get('/posts/:pid/comments', handler.getCommentsByPid);

router.get('/comments', handler.getComments);
router.post('/comments', handler.createComment);
router.get('/comments/:cid', handler.getCommentById);
router.put('/comments/:cid', handler.editComment);
router.delete('/comments/:cid', handler.deleteComment);
// router.get('/comments/:cid/replies', handler.getRepliesByCid);
// router.post('/comments/:cid/replies', handler.createReply);

// router.get('/replies', handler.getReplies);
// router.get('/replies/:rid', handler.getReplyById);
// router.put('/replies/:rid', handler.editReply);
// router.delete('/replies/:rid', handler.deleteReply);

export default router;
