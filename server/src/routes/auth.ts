import { Router } from 'express';
import { getAuthParams } from '../utils/middlewares';
import { login, callback, logout } from '../handlers/auth';

const router = Router();
router.use(getAuthParams);

// The login endpoint initiates the OAuth PKCE process
router.get('/login', login);
router.get('/callback', callback);
router.get('/logout', logout);

export default router;
