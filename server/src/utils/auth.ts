import crypto from 'crypto';
import { AuthUrlParams } from '../types/auth';

export const generateCodeChallenge = () => {
    const codeVerifier = crypto.randomBytes(64).toString('hex');
    const codeChallenge = crypto.createHash('sha256').update(codeVerifier).digest('base64url');
    const state = crypto.randomBytes(16).toString('hex');

    return { codeVerifier, codeChallenge, state };
};

export const generateAuthUrl = ({ clientId, redirectUri, scopes, codeChallenge, state }: AuthUrlParams) => {
    const authUrl = new URL('https://accounts.spotify.com/authorize');
    authUrl.searchParams.append('client_id', clientId);
    authUrl.searchParams.append('response_type', 'code');
    authUrl.searchParams.append('redirect_uri', redirectUri);
    authUrl.searchParams.append('scope', scopes.join(' '));
    authUrl.searchParams.append('code_challenge_method', 'S256');
    authUrl.searchParams.append('code_challenge', codeChallenge);
    authUrl.searchParams.append('state', state);

    return authUrl.toString();
};
