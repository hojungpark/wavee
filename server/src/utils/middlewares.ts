import { Request, Response, NextFunction } from 'express-serve-static-core';
import { Pool } from 'pg';
import { SpotifyApi, AccessToken } from '@spotify/web-api-ts-sdk';
import { connectLocalSQL, connectCloudSQL } from './connect';
import { getEnv } from './env';

declare module 'express-serve-static-core' {
    interface Request {
        pool?: Pool;
        close?: () => Promise<void>;
        sdk?: SpotifyApi;
        clientId?: string;
        redirectUri?: string;
        scopes?: string[];
    }
}

export const getAuthParams = (req: Request, res: Response, next: NextFunction) => {
    const clientId = getEnv('SPOTIFY_CLIENT_ID');

    const redirectUri =
        getEnv('NODE_ENV') === 'development'
            ? `http://localhost:${getEnv('PORT')}/auth/callback`
            : `${getEnv('URL')}/auth/callback`;

    const scopes = [
        'user-read-private',
        'user-read-email',
        'user-library-read',
        'user-top-read',
        'playlist-read-private',
        'playlist-read-collaborative',
        'playlist-modify-public',
        'playlist-modify-private',
    ];

    req.clientId = clientId;
    req.redirectUri = redirectUri;
    req.scopes = scopes;

    next();
};

export const requestSpotifySDK = (req: Request, res: Response, next: NextFunction) => {
    if (!req.clientId) {
        return res.status(510).json({ message: 'Spotify Client ID is not extended in req.' });
    }
    const clientId = req.clientId!;

    if (!req.session.accessTokenData) {
        return res.status(401).json({ message: 'Access token data is missing' });
    }
    const accessTokenData = req.session.accessTokenData;

    const accessTokenObject: AccessToken = {
        access_token: accessTokenData.access_token,
        token_type: 'Bearer',
        expires_in: accessTokenData.expires_in,
        refresh_token: accessTokenData.refresh_token || '',
    };
    req.sdk = SpotifyApi.withAccessToken(clientId, accessTokenObject);

    next();
};

export type LocalSQLParams = {
    user: string;
    password: string;
    database: string;
    host: string;
    port: number;
};

export type CloudSQLParams = {
    user: string;
    password: string;
    database: string;
    instanceConnectionName: string;
};

export const dbConnectSQL = async (req: Request, res: Response, next: NextFunction) => {
    if (getEnv('NODE_ENV') === 'development') {
        dbConnectLocalSQL(req, res, next);
    } else {
        await dbConnectCloudSQL(req, res, next);
    }
};

const dbConnectCloudSQL = async (req: Request, res: Response, next: NextFunction) => {
    const params = {
        user: getEnv('DB_CLOUD_USER'),
        password: getEnv('DB_CLOUD_PASSWORD'),
        database: getEnv('DB_CLOUD_DATABASE'),
        instanceConnectionName: getEnv('DB_CLOUD_INSTANCE_CONNECTION_NAME'),
    };
    const { pool, close } = await connectCloudSQL(params);
    req.pool = pool;
    req.close = close;
    next();
};

const dbConnectLocalSQL = (req: Request, res: Response, next: NextFunction) => {
    const params = {
        user: getEnv('DB_LOCAL_USER'),
        password: getEnv('DB_LOCAL_PASSWORD'),
        database: getEnv('DB_LOCAL_DATABASE'),
        host: getEnv('DB_LOCAL_HOST'),
        port: Number(getEnv('DB_LOCAL_PORT')),
    };
    const { pool } = connectLocalSQL(params);
    req.pool = pool;
    next();
};
