import { IpAddressTypes, Connector } from '@google-cloud/cloud-sql-connector';
import { Pool } from 'pg';
import type { LocalSQLParams, CloudSQLParams } from './middlewares';

export const connectCloudSQL = async ({ user, password, database, instanceConnectionName }: CloudSQLParams) => {
    const connector = new Connector();
    const clientOpts = await connector.getOptions({
        instanceConnectionName,
        ipType: IpAddressTypes.PUBLIC,
    });

    const pool = new Pool({
        ...clientOpts,
        user: user,
        password: password,
        database: database,
    });

    return {
        pool,
        async close() {
            await pool.end();
            connector.close();
        },
    };
};

export const connectLocalSQL = ({ user, password, database, host, port }: LocalSQLParams) => {
    const pool = new Pool({
        user: user,
        password: password,
        database: database,
        host: host,
        port: port,
    });

    return {
        pool,
    };
};
