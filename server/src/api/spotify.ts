import axios from 'axios';
import { SpotifyApi } from '@spotify/web-api-ts-sdk';

declare module 'express-serve-static-core' {
    interface Request {
        spotifyApi?: SpotifyApi;
    }
}

export const requestAccessToken = async (params: URLSearchParams) => {
    const response = await axios.post('https://accounts.spotify.com/api/token', params.toString(), {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
        },
    });
    return response.data;
};

export const requestUserProfile = async (accessToken: string) => {
    try {
        const response = await axios.get('https://api.spotify.com/v1/me', {
            headers: { Authorization: `Bearer ${accessToken}` },
        });

        return response.data;
    } catch (err) {
        console.error(err);
    }
};

export const getCurrentUserProfile = async (sdk: SpotifyApi) => {
    try {
        const currentUserProfile = await sdk.currentUser.profile();
        return currentUserProfile;
    } catch (err) {
        console.error(err);
    }
};

export const getCurrentUserPlaylists = async (sdk: SpotifyApi) => {
    try {
        const currentUserPlaylists = await sdk.currentUser.playlists.playlists();
        return currentUserPlaylists;
    } catch (err) {
        console.error(err);
    }
};

export const getCurrentUserSavedAlbums = async (sdk: SpotifyApi) => {
    try {
        const currentUserSavedAlbums = await sdk.currentUser.albums.savedAlbums();
        return currentUserSavedAlbums;
    } catch (err) {
        console.error(err);
    }
};

export const getCurrentUserTopArtists = async (sdk: SpotifyApi) => {
    try {
        const currentUserTopArtists = await sdk.currentUser.topItems('artists');
        return currentUserTopArtists;
    } catch (err) {
        console.error(err);
    }
};

export const getCurrentUserTopTracks = async (sdk: SpotifyApi) => {
    try {
        const currentUserTopTracks = await sdk.currentUser.topItems('tracks');
        return currentUserTopTracks;
    } catch (err) {
        console.error(err);
    }
};

export const getFeaturedPlaylists = async (sdk: SpotifyApi) => {
    try {
        const featuredPlaylists = await sdk.browse.getFeaturedPlaylists();
        return featuredPlaylists;
    } catch (err) {
        console.error(err);
    }
};

export const getNewReleases = async (sdk: SpotifyApi) => {
    try {
        const newReleases = await sdk.browse.getNewReleases();
        return newReleases;
    } catch (err) {
        console.error(err);
    }
};

export const getUserProfile = async (sdk: SpotifyApi, userId: string) => {
    try {
        const userProfile = await sdk.users.profile(userId);
        return userProfile;
    } catch (err) {
        console.error(err);
    }
};

export const getAlbum = async (sdk: SpotifyApi, albumId: string) => {
    try {
        const album = await sdk.albums.get(albumId);
        return album;
    } catch (err) {
        console.error(err);
    }
};

export const getAlbumTracks = async (sdk: SpotifyApi, albumId: string) => {
    try {
        const albumTracks = await sdk.albums.tracks(albumId);
        return albumTracks;
    } catch (err) {
        console.error(err);
    }
};

export const getPlaylist = async (sdk: SpotifyApi, playlistId: string) => {
    try {
        const playlist = await sdk.playlists.getPlaylist(playlistId);
        return playlist;
    } catch (err) {
        console.error(err);
    }
};

export const getPlaylistTracks = async (sdk: SpotifyApi, playlistId: string) => {
    try {
        const playlistTracks = await sdk.playlists.getPlaylistItems(playlistId);
        return playlistTracks;
    } catch (err) {
        console.error(err);
    }
};

export const getTrack = async (sdk: SpotifyApi, trackId: string) => {
    try {
        const track = await sdk.tracks.get(trackId);
        return track;
    } catch (err) {
        console.error(err);
    }
};
