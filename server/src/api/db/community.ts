import { Pool } from 'pg';

export const getPosts = async (pool: Pool) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "posts" ORDER BY modified_at DESC');
        return rows;
    } catch (error) {
        console.error('db.getPosts error:', error);
    }
};

export const createPost = async (pool: Pool, title: string, body: string, spotify_user_id: string) => {
    try {
        const query = 'INSERT INTO "posts"(title, body, spotify_user_id) VALUES ($1, $2, $3) RETURNING *';
        const { rows } = await pool.query(query, [title, body, spotify_user_id]);
        return rows[0];
    } catch (error) {
        console.error('db.createPost error:', error);
    }
};

export const getPostById = async (pool: Pool, pid: string) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "posts" WHERE id=$1', [pid]);
        return rows[0];
    } catch (error) {
        console.error('db.getPostById error:', error);
    }
};

export const editPost = async (pool: Pool, pid: string, title: string, body: string) => {
    try {
        const query = `
            UPDATE "posts" 
            SET title=$1, body=$2, modified_at=now()
            WHERE id=$3 RETURNING *
        `;
        const { rows } = await pool.query(query, [title, body, pid]);
        return rows;
    } catch (error) {
        console.error('db.editPost error:', error);
    }
};

export const deletePost = async (pool: Pool, pid: string) => {
    try {
        const { rows } = await pool.query('DELETE FROM "posts" WHERE id=$1 RETURNING *', [pid]);
        return rows;
    } catch (error) {
        console.error('db.deletePost error:', error);
    }
};

export const getCommentsByPid = async (pool: Pool, pid: string) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "comments" WHERE post_id=$1 ORDER BY modified_at DESC', [pid]);
        return rows;
    } catch (error) {
        console.error('db.getCommentsByPid error:', error);
    }
};

export const createComment = async (pool: Pool, pid: string, body: string, spotify_user_id: string) => {
    try {
        const query = 'INSERT INTO "comments"(body, post_id, spotify_user_id) VALUES ($1, $2, $3) RETURNING *';
        const { rows } = await pool.query(query, [body, pid, spotify_user_id]);
        return rows[0];
    } catch (error) {
        console.error('db.createComment error:', error);
    }
};

export const getComments = async (pool: Pool) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "comments" ORDER BY modified_at DESC');
        return rows;
    } catch (error) {
        console.error('db.getComments error:', error);
    }
};

export const getCommentById = async (pool: Pool, cid: string) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "comments" WHERE id=$1', [cid]);
        return rows[0];
    } catch (error) {
        console.error('db.getCommentById error:', error);
    }
};

export const editComment = async (pool: Pool, cid: string, body: string) => {
    try {
        const query = `
            UPDATE "comments" 
            SET body=$1, modified_at=now()
            WHERE id=$2
            RETURNING *
        `;
        const { rows } = await pool.query(query, [body, cid]);
        return rows;
    } catch (error) {
        console.error('db.editComment error:', error);
    }
};

export const deleteComment = async (pool: Pool, cid: string) => {
    try {
        const { rows } = await pool.query('DELETE FROM "comments" WHERE id=$1 RETURNING *', [cid]);
        return rows;
    } catch (error) {
        console.error('db.deleteComment error:', error);
    }
};

export const getRepliesByCid = async (pool: Pool, cid: string) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "replies" WHERE comment_id=$1 ORDER BY modified_at DESC', [
            cid,
        ]);
        return rows;
    } catch (error) {
        console.error('db.getRepliesByCid error:', error);
    }
};

export const createReply = async (pool: Pool, cid: string, body: string, spotify_user_id: string) => {
    try {
        const query = 'INSERT INTO "replies"(body, comment_id, spotify_user_id) VALUES ($1, $2, $3) RETURNING *';
        const { rows } = await pool.query(query, [body, cid, spotify_user_id]);
        return rows[0];
    } catch (error) {
        console.error('db.createReply error:', error);
    }
};

export const getReplies = async (pool: Pool) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "replies" ORDER BY modified_at DESC');
        return rows;
    } catch (error) {
        console.error('db.getReplies error:', error);
    }
};

export const getReplyById = async (pool: Pool, rid: string) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "replies" WHERE id=$1', [rid]);
        return rows[0];
    } catch (error) {
        console.error('db.getReplyById error:', error);
    }
};

export const editReply = async (pool: Pool, rid: string, body: string) => {
    try {
        const query = `
            UPDATE "comments" 
            SET body=$1, modified_at=now()
            WHERE id=$2
            RETURNING *
        `;
        const { rows } = await pool.query(query, [body, rid]);
        return rows;
    } catch (error) {
        console.error('db.editReply error:', error);
    }
};

export const deleteReply = async (pool: Pool, rid: string) => {
    try {
        const { rows } = await pool.query('DELETE FROM "replies" WHERE id=$1 RETURNING *', [rid]);
        return rows;
    } catch (error) {
        console.error('db.deleteReply error:', error);
    }
};
