import { Pool } from 'pg';

export const getAlbumComments = async (pool: Pool) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "albumComments" ORDER BY modified_at DESC');
        return rows;
    } catch (error) {
        console.error('db.getAlbumComments error:', error);
    }
};

export const getAlbumCommentsByAid = async (pool: Pool, aid: string) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "albumComments" WHERE album_id=$1 ORDER BY modified_at DESC', [
            aid,
        ]);
        return rows;
    } catch (error) {
        console.error('db.getAlbumCommentsByAid error:', error);
    }
};

export const createAlbumComment = async (pool: Pool, aid: string, body: string, spotify_user_id: string) => {
    try {
        const query = 'INSERT INTO "albumComments"(body, album_id, spotify_user_id) VALUES ($1, $2, $3) RETURNING *';
        const { rows } = await pool.query(query, [body, aid, spotify_user_id]);
        return rows[0];
    } catch (error) {
        console.error('db.createAlbumComment error:', error);
    }
};

export const getAlbumCommentByCid = async (pool: Pool, cid: string) => {
    try {
        const { rows } = await pool.query('SELECT * FROM albumComments WHERE id=$1 ORDER BY modified_at DESC', [cid]);
        return rows;
    } catch (error) {
        console.error('db.getAlbumCommentByCid error:', error);
    }
};

export const editAlbumComment = async (pool: Pool, cid: string, body: string) => {
    try {
        const query = `
            UPDATE "albumComments" 
            SET body=$1, modified_at=now()
            WHERE id=$2
            RETURNING *
        `;
        const { rows } = await pool.query(query, [body, cid]);
        return rows;
    } catch (error) {
        console.error('db.editAlbumComment error:', error);
    }
};

export const deleteAlbumComment = async (pool: Pool, cid: string) => {
    try {
        const { rows } = await pool.query('DELETE FROM "albumComments" WHERE id=$1 RETURNING *', [cid]);
        return rows;
    } catch (error) {
        console.error('db.deleteAlbumComment error:', error);
    }
};
