import { Pool } from 'pg';

export const getPlaylistComments = async (pool: Pool) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "playlistComments" ORDER BY modified_at DESC');
        return rows;
    } catch (error) {
        console.error('db.getPlaylistComments error:', error);
    }
};

export const getPlaylistCommentsByPid = async (pool: Pool, pid: string) => {
    try {
        const { rows } = await pool.query(
            'SELECT * FROM "playlistComments" WHERE playlist_id=$1 ORDER BY modified_at DESC',
            [pid],
        );
        return rows;
    } catch (error) {
        console.error('db.getPlaylistCommentsByAid error:', error);
    }
};

export const createPlaylistComment = async (pool: Pool, pid: string, body: string, spotify_user_id: string) => {
    try {
        const query =
            'INSERT INTO "playlistComments"(body, playlist_id, spotify_user_id) VALUES ($1, $2, $3) RETURNING *';
        const { rows } = await pool.query(query, [body, pid, spotify_user_id]);
        return rows[0];
    } catch (error) {
        console.error('db.createPlaylistComment error:', error);
    }
};

export const getPlaylistCommentByCid = async (pool: Pool, cid: string) => {
    try {
        const { rows } = await pool.query('SELECT * FROM "playlistComments" WHERE id=$1 ORDER BY modified_at DESC', [
            cid,
        ]);
        return rows;
    } catch (error) {
        console.error('db.getPlaylistCommentsByCid error:', error);
    }
};

export const editPlaylistComment = async (pool: Pool, cid: string, body: string) => {
    try {
        const query = `
            UPDATE "playlistComments" 
            SET body=$1, modified_at=now()
            WHERE id=$2
            RETURNING *
        `;
        const { rows } = await pool.query(query, [body, cid]);
        return rows;
    } catch (error) {
        console.error('db.editPlaylistComment error:', error);
    }
};

export const deletePlaylistComment = async (pool: Pool, cid: string) => {
    try {
        const { rows } = await pool.query('DELETE FROM "playlistComments" WHERE id=$1 RETURNING *', [cid]);
        return rows;
    } catch (error) {
        console.error('db.deleteComment error:', error);
    }
};
