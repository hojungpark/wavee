export interface CreatePostDto {
    title: string;
    content: string;
    spotify_user_id: string;
}

export interface EditPostDto {
    title: string;
    content: string;
}
