export interface CallbackDto {
    clientId: string;
    redirectUri: string;
}
